const Sequelize = require('sequelize');

const CONFIG = require('../config/config');

const sequelize = new Sequelize(CONFIG.db_name, CONFIG.db_user, CONFIG.db_password, {
  host: CONFIG.db_host,
  dialect: CONFIG.db_dialect,
  port: CONFIG.db_port,
  operatorsAliases: false,
  logging: CONFIG.app == 'dev' ? console.log : false
});

// Register models
const UserModel = sequelize.import("./user.model");

// Model relationships

// Sync all model schemas to the database
sequelize.sync();

const db = {
  sequelize,
  models: {
    user: UserModel
  }
}

module.exports = db;