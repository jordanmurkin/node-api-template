/**
 * User model definition for Sequelize
 * 
 * @name models/user
 * @model User
 * 
 * @property id {UUID} - Identifier of the user
 * @property email {string} - Unique email for the user
 * @property firstName {string} - First name of the user
 * @property lastName {string} - Last name of the user
 * @property password {string} - Password of the user, hashed using bcrypt
 * @property role {string} - User's role
 */

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuidv4 = require('uuid/v4');

const CONFIG = require('../config/config');
const ROLES = require('../config/types/roles');
const TOKENS = require('../config/types/tokens');

module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define("user", {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING
  }, {
    timestamps: true,
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at'
  });

  Model.beforeCreate(async (user, options) => {
    user.id = uuidv4();
  });

  Model.beforeSave(async (user, options) => {
    if (user.changed('password')) {
      const hash = await bcrypt.hash(user.password, 10);
      user.password = hash;
    }

    if (user.changed('first_name')) {
      const lowercase_firstName = user.first_name.trim().toLowerCase()
      user.first_name = lowercase_firstName.charAt(0).toUpperCase() +  lowercase_firstName.slice(1);
    }

    if (user.changed('last_name')) {
      const lowercase_lastName = user.last_name.trim().toLowerCase()
      user.last_name = lowercase_lastName.charAt(0).toUpperCase() +  lowercase_lastName.slice(1);
    }
  });

  Model.prototype.comparePassword = function (plainText) {
    return new Promise((resolve, reject) => {
      bcrypt.compare(plainText, this.password)
      .then((isMatch) => {
        resolve(isMatch);
      })
      .catch(error => {
        reject(error);
      })
    });
  }

  Model.prototype.isAdmin = function () {
    return this.role == ROLES.ADMIN_ROLE;
  }

  Model.prototype.getAccessToken = function () {
    const expiration_time = parseInt(CONFIG.jwt_access_expiry_time);

    const payload = {
      user_id: this.id,
      type: TOKENS.ACCESS_TOKEN
    };

    return jwt.sign(payload, CONFIG.jwt_encryption_key, { expiresIn: expiration_time });
  };

  Model.prototype.getRefreshToken = function () {
    const expiration_time = parseInt(CONFIG.jwt_refresh_expiry_time);

    const payload = {
      user_id: this.id,
      type: TOKENS.REFRESH_TOKEN
    };

    return jwt.sign(payload, CONFIG.jwt_encryption_key, { expiresIn: expiration_time });
  };

  Model.prototype.getPrivateDetails = function () {
    const details = {
      user_id: this.id,
      email: this.email,
      first_name: this.first_name,
      last_name: this.last_name,
      role: this.role,
    };

    return details;
  }

  Model.prototype.getPublicDetails = function () {
    const details = {
      user_id: this.id,
      first_name: this.first_name,
      last_name: this.last_name
    };

    return details;
  }

  return Model;
}