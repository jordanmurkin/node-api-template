const has = require('has');

/**
 * The ResponseBuilder provides methods to create standardised responses for the API
 */
class ResponseBuilder {
  /**
   * Build a response from an unsuccessful API call
   * 
   * @param {*} error
   * 
   * @returns {Object}
   */
  static buildErrorResponse(error) {
    error = (has(error, 'message')) ? error.message : error;

    return {
      status: "ERROR",
      error: error
    }
  }

  /**
   * Build a response from a successful API call
   * 
   * @param {*} data 
   * 
   * @returns {Object}
   */
  static buildSuccessResponse(data) {
    return {
      status: "OK",
      data: data
    }
  }
}

module.exports = ResponseBuilder;