/**
 * User express routes. Provides endpoints to retrieve and manipulate data
 * relating to the authenticated user (from the JWT token)
 * 
 * @module User
 */

const express = require('express');
const passport = require('passport');
const { check, oneOf } = require('express-validator/check');
const has = require('has');

const rb = require('../lib/ResponseBuilder');
const UserController = require('../controllers/user.controller');

const { handleValidationError } = require('../middleware/error');

const user = express.Router();

/**
 * Get basic user details 
 * 
 * @name /user
 * @route {GET} /user
 * 
 * @authentication This route requires HTTP Bearer Authentication using the JWT token from the /auth/login route.
 * 
 * @example
 * {
 *    "status": "OK",
 *    "data": {
 *      "user_id": "2814ac9c-36a4-4691-8885-5bdea4030418",
 *      "email": "jordan@murkin.me",
 *      "first_name": "Jordan",
 *      "last_name": "Murkin",
 *      "role": "ADMIN_ROLE",
 *    }
 * }
 * 
 * @returns {string}
 */
user.get('/', passport.authenticate('jwt', { session: false, failWithError: true }), function (req, res) {
  const user = req.user; // from passport authentication

  const private_details = user.getPrivateDetails();
  
  return res.json(rb.buildSuccessResponse(private_details));
});

module.exports = {
  path: '/user',
  router: user
}