/**
 * Authentication module providing express routes for the user to authenticate with the API
 * 
 * @module Authentication
 */

const express = require('express');
const passport = require('passport');

const rb = require('../lib/ResponseBuilder');

const { check, oneOf } = require('express-validator/check');
const { handleValidationError } = require('../middleware/error');

const auth = express.Router();

/**
 * Retrieve a JWT token for a user given their email and password.
 * 
 * @name /auth/login
 * @route {POST} /auth/login
 * 
 * @bodyparam {string} [email] - The email of the user.
 * @bodyparam {string} [password] - The password of the user.
 * 
 * @example
 * {
 *    "status": "OK",
 *    "data": {
 *        "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiOTQ1NWQxM2ItMmIwZS00YzA1LTlkYmUtNWI5ZDJmOTRkOTdlIiwidHlwZSI6IkFDQ0VTU19UT0tFTiIsImlhdCI6MTU0ODA3MzI1MywiZXhwIjoxNTQ4MTA5MjUzfQ.OrqsPH-YiS7_cjX-aAqY5dM64YktoUzn5PYpPU8jimo",
 *        "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiOTQ1NWQxM2ItMmIwZS00YzA1LTlkYmUtNWI5ZDJmOTRkOTdlIiwidHlwZSI6IlJFRlJFU0hfVE9LRU4iLCJpYXQiOjE1NDgwNzMyNTMsImV4cCI6MTU0ODQzMzI1M30.Kmpdtg-NqyVxCHYv9WpZxpbm4KvOqmNPEK4q-Zh0mx4"
 *    }
 * }
 * 
 * @returns {string}
 */
auth.post(
  '/login', [
    passport.authenticate(['local'], { session: false, failWithError: true })
  ], function (req, res) {
    const user = req.user;
    
    const access_token = user.getAccessToken();
    const refresh_token = user.getRefreshToken();
    const private_details = user.getPrivateDetails();

    return res.json(rb.buildSuccessResponse({
      access_token,
      refresh_token,
      user: private_details
    }));
})

module.exports = {
  path: '/auth',
  router: auth
}