const ADMIN_ROLE = "ADMIN_ROLE"; // System administrators
const USER_ROLE = "USER_ROLE"; // System users

module.exports = {
  ADMIN_ROLE,
  USER_ROLE
}