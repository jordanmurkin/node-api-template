require('dotenv').config(); // Instatiate environment variables

let CONFIG = {}

CONFIG.app = process.env.APP || 'dev';
CONFIG.port = process.env.PORT || '3000';

CONFIG.db_dialect = process.env.DB_DIALECT || 'mysql';
CONFIG.db_host = process.env.DB_HOST || 'localhost';
CONFIG.db_port = process.env.DB_PORT || '3306';
CONFIG.db_name = process.env.DB_NAME || 'db_name';
CONFIG.db_user = process.env.DB_USER || 'db_user';
CONFIG.db_password = process.env.DB_PASSWORD || 'db_password';

CONFIG.jwt_encryption_key = process.env.JWT_ENCRYPTION_KEY || 'CGN28HUNQ8980NS3HFIOW8N34';
CONFIG.jwt_access_expiry_time = process.env.JWT_ACCESS_EXPIRY_TIME || '86400'; // 1 day
CONFIG.jwt_refresh_expiry_time = process.env.JWT_REFRESH_EXPIRY_TIME || '2592000'; // 30 days

module.exports = CONFIG;