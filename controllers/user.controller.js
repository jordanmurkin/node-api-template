const User = require('../models/db').models.user;
const ROLES = require('../config/types/roles');

/**
 * Validates an email/password exists in the database and returns the user
 * 
 * @param {string} email 
 * @param {string} password 
 */
const login = (email, password) => {
  return new Promise((resolve, reject) => {
    let user = null;

    User.findOne(
      {
        where: {
          "email": email
        }
      }
    )
    .then(databaseUser => {
      user = databaseUser;

      if (!user) throw new Error("User does not exist");

      return user.comparePassword(password);
    })
    .then(isMatch => {
      if (!isMatch) throw new Error("Password does not match");

      resolve(user);
    })
    .catch(error => {
      reject(error);
    });
  });
}

module.exports.login = login;

/**
 * Create a new user
 * 
 * @param {*} userInfo 
 * @param {*} opts 
 */
const create = (userInfo, opts = {}) => {
  if (opts.admin) {
    userInfo.role = ROLES.ADMIN_ROLE;
  } else {
    userInfo.role = ROLES.USER_ROLE;
  }

  return User.create(userInfo);
}

module.exports.create = create;

/**
 * Find one user by id
 */
const findById = (id) => {
  return User.findOne(
    {
      where: {
        "id": id
      }
    }
  );
}

module.exports.findById = findById;