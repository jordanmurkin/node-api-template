const UserController = require('../controllers/user.controller');

const defaultUsers = [];

const adminUser = {
  email: 'admin@emailserver.com',
  first_name: 'Admin',
  last_name: 'Account',
  password: 'password'
}

defaultUsers.push({
  info: adminUser,
  opts: { admin: true }
});

const basicUser = {
  email: 'user@emailserver.com',
  first_name: 'User',
  last_name: 'Account',
  password: 'password'
}

defaultUsers.push({
  info: basicUser,
  opts: {}
});

const promises = [];

defaultUsers.forEach(user => {
  promises.push(UserController.create(user.info, user.opts));
})

Promise.all(promises)
.then(() => {
  console.log("Success");
  process.exit(0);
})
.catch(error => {
  console.error(error);
})