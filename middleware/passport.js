const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const LocalStrategy = require('passport-local').Strategy;

const has = require('has');

const CONFIG = require('../config/config');

const UserController = require('../controllers/user.controller');

/**
 * Adds four custom authentication strategies to Passport.
 * 'jwt' - ensuring that the JWT is valid and that the user exists
 * 'local' - Authentication is performed on username/password in the request
 * 
 * @param passport   the passport instance to apply the middleware to
 */
module.exports = function (passport) {
  const access_opts = {};
  access_opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  access_opts.secretOrKey = CONFIG.jwt_encryption_key;

  passport.use('jwt', new JwtStrategy(access_opts, async function (jwt_payload, done) {
    // find user by jwt_payload.user_id
    let user_id = null;

    if (has(jwt_payload, 'user_id')) {
      user_id = jwt_payload.user_id;
    }

    UserController.findById(user_id)
    .then(user => {
      return done(null, user);
    })
    .catch(error => {
      return done(error, false);
    })
  }));

  passport.use('local', new LocalStrategy({
      usernameField: 'email',
      passwordField: 'password',
    }, async (email, password, done) => {
      UserController.login(email, password)
      .then(user => {
        return done(null, user);
      })
      .catch(error => {
        return done(error, false);
      });
  }));
}