const { validationResult } = require('express-validator/check');

const rb = require('../lib/ResponseBuilder');
const has = require('has');

/**
 * Custom express middleware to handle all validation errors raised by express-validator
 */
const handleValidationError = function(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.json(rb.buildErrorResponse(errors.array()));
  }

  next();
}

/**
 * Custom express middleware to handle all requests to routes that do not exist (404)
 */
const notFound = function(req, res) {
  return res.json(rb.buildErrorResponse("Route not found"));
}

/**
 * Custom express middleware that catches all unhandled errors 
 */
const serverError = function(error, req, res, next) {
  // if error codes are not required, commment here
  if (has(error, 'status')) {
    res.statusCode = error.status;
  }

  return res.json(rb.buildErrorResponse(error));
}

module.exports = {
  handleValidationError,
  notFound,
  serverError
};