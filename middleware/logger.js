/**
 * Custom express middleware to log all requests that the server receives
 */
const loggerMiddleware = function(req, res, next) {
  console.log(`${new Date()} ${req.method} ${req.url} ${JSON.stringify(req.body)}`);
  next();
}

module.exports = loggerMiddleware;