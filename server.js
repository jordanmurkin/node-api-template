const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');

const CONFIG = require('./config/config');

const app = express();

// Express Middleware
const logger = require('./middleware/logger');
const { notFound, serverError } = require('./middleware/error');
const corsMiddleware = require('./middleware/cors');

// Passport middleware
require('./middleware/passport')(passport);

// Express Router Modules
const auth = require('./modules/authentication');
const user = require('./modules/user');

app.use(corsMiddleware);
app.use(bodyParser.json());
app.use(passport.initialize());

if (CONFIG.app == 'dev') {
  app.use(logger);
}

// Load Express Router Modules
app.use(auth.path, auth.router);
app.use(user.path, user.router);

// Error handling
app.use(notFound);
app.use(serverError);

// List all available routes (usability function - optional)
const expressRouters = [auth, user];
expressRouters.forEach(expressRouter => {
  console.log(`=== ${expressRouter.path} ===`);
  expressRouter.router.stack.forEach(function (r) {
    if (r.route && r.route.path) {
      if (r.route.methods.post) {
        console.log(`POST ${expressRouter.path}${r.route.path}`);
      }
      if (r.route.methods.get) {
        console.log(`GET ${expressRouter.path}${r.route.path}`);
      }
    }
  });
});

// Start server
console.log(`=== LISTENING ON PORT ${CONFIG.port} ===`);
app.listen(CONFIG.port);